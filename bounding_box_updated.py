import numpy as np
import cv2
import os
import pathlib as path
window_name = 'Image'
f = open('annotations.txt',"r")
text = f.readlines()
for lines in text:
        line = lines.strip()
        words = line.split(" ")#splits line into words based on space
        A = np.array(words)#coverts the words into arrays(array holds the image,No of BB,coordinate value)
        print(A)#displays the line in annotation file as array
        path1 = os.path.join(os.getcwd(),'images/')
        b = path1 + A[0]
        output = cv2.imread(b)#reads image from annotation file
        color = (0, 20, 10)  # specifies the colour(RGB) of bounding box
        thickness = 3  # the rectangular box will have thickness 3
        # build in opencv function cv2.rectangle can be used to draw a rectangle box at the specified points in image
        if(int(A[1])==1):#check for the number of bounding box, in this case draw 1 BB in image
                output = cv2.rectangle(output, (int(A[2]), int(A[3])), (int(A[4]), int(A[5])), color, thickness)
        elif(int(A[1])==2):#check for the number of bounding box, in this case draw 2 BB in image
                output = cv2.rectangle(output, (int(A[2]), int(A[3])), (int(A[4]),int(A[5])),color, thickness)
                output = cv2.rectangle(output, (int(A[6]), int(A[7])),(int(A[8]), int(A[9])), color, thickness)
        cv2.imshow(window_name, output)
        cv2.waitKey(1000)
                        
      
