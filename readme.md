# Code bounding_box.py
This code creates bounding boxes on images and displays them. 
Bounding boxes are rectangles that appear coordinates specified in the code.

###IMPORTANT

Change the path directory to the images in accordance to image folder path in your computer.

### The annotation file

The annotation file contains information about the images. Each line
corresponds to an image. The components of a line are separated by a space. The
first column is the name of the image. The second is the number of bounding
boxes detected. The rest of the columns contain the coordinates of the bounding
boxes: `xmin ymin xmax ymax`, where the point `(xmin ymin)` is the top-left
corner and the point `(xmax ymax)` the bottom-right corner of the bounding box.
If there is more than one bounding box, other coordinates can follow.

For example for an image with two bounding boxes:
`image_name 2 xmin0 ymin0 xmax0 ymax0 xmin1 ymin1 xmax1 ymax1`







