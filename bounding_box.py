import cv2 #imports the opencv package
# path of the image file
image_path1 = r'/home/vaishali/assignment_bboxes/images/000005.png'
image_path2 = r'/home/vaishali/assignment_bboxes/images/000009.png'
image_path3 = r'/home/vaishali/assignment_bboxes/images/000010.png'
image_path4 = r'/home/vaishali/assignment_bboxes/images/000011.png'
image_path5 = r'/home/vaishali/assignment_bboxes/images/000014.png'
#creating a list which contains all the image paths
img_loc = [image_path1,image_path2,image_path3,image_path4,image_path5]
#naming the window in which image will be displayed
window_name = 'Image'
#creating a list that contains the startpoint of all images (xmin,ymin) and end points as (xmax,ymax)
#uses the values form annotations.txt
box_start_points = [(157, 62), (12, 34), (373, 205), (100, 259), (121, 60), (51, 116), (241, 296)]
box_end_points = [(403, 225), (475, 332), (401, 253), (170, 301), (474, 272), (248, 264), (310, 331)]
#initially set i to index value 0
i = 0
#loop to iterate the images and display it with rectangle boxes
for image in img_loc:
    output = cv2.imread(image)
    color = (0, 20, 10) #specifies the colour(RGB) of bounding box
    thickness = 3 #the rectangular box will have thickness 3
    #build in opencv function cv2.rectangle can be used to draw a rectangle box at the specified points in image
    output= cv2.rectangle(output, box_start_points[i], box_end_points[i], color, thickness)
    cv2.imshow(window_name, output)#displays the image
    cv2.waitKey(600)#display time of the image
    #since image 3 and 4 has to display two bounding box per image
    if image == image_path3 or image == image_path4:
        output = cv2.rectangle(output, box_start_points[i + 1], box_end_points[i + 1], color, thickness)
        cv2.imshow(window_name, output)
        cv2.waitKey(1000)
        i += 1
    i += 1









